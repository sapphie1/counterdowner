use chrono::offset::{TimeZone, Utc};
use chrono::{DateTime, Duration, NaiveDateTime};
use chrono_tz::Tz;
use derive_more::*;
use gloo::console;
use gloo::timers::callback::Interval;
use gloo_net::http::Request;
use serde::Deserialize;
use std::fmt::Display;
use std::num::NonZeroU64;
use yew::prelude::*;

#[derive(Default, Clone, Copy, PartialEq)]
struct MyDuration {
    h: u8,
    m: u8,
    s: u8,
    d: u64,
}

const CONV: &[u64] = &[
    60, // seconds in a minute
    60, // minutes in an hour
    24, // hours in a day
];

impl MyDuration {
    /// Custom conversion because it's easier
    fn from_secs(mut s: u64) -> Self {
        let mut values = [0, 0, 0];
        for (value, conv_fct) in values.iter_mut().zip(CONV) {
            *value = (s % conv_fct) as u8;
            s /= conv_fct;
        }
        let d = s;
        let [s, m, h] = values;
        MyDuration { h, m, s, d }
    }
}

struct App {
    state: State,
}

enum State {
    Loading,
    Loaded {
        when: DateTime<Utc>,
        _interval_handle: Interval,
    },
    Err,
}

enum Message {
    UpdateTime,
    LoadConfig(DateTime<Utc>),
    Error,
}

impl Component for App {
    type Message = Message;

    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let window = web_sys::window();
        let prefix = {
            let mut prefix = window
                .and_then(|window| window.location().pathname().ok())
                .unwrap_or_else(|| "/".to_string());
            if let Some(last_slash) = prefix.rfind(|c| c == '/') {
                prefix.truncate(last_slash + 1)
            } else {
                prefix.clear();
                prefix.push('/');
            };
            prefix
        };
        console::log!(&prefix);

        {
            let link = ctx.link().clone();
            wasm_bindgen_futures::spawn_local(async move {
                match load_config(prefix).await {
                    Ok(config) => link.send_message(Message::LoadConfig(config)),
                    Err(e) => {
                        console::log!("Error: {}", e.to_string());
                        link.send_message(Message::Error)
                    }
                };
            })
        };
        App {
            state: State::Loading,
        }
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        match self.state {
            State::Loaded { when, .. } => {
                let secs_remaining = secs_remaining(when);
                let inner: Html = match secs_remaining {
                    Some(total_secs) => {
                        let until_then = MyDuration::from_secs(total_secs.get());
                        let MyDuration { h, m, s, d } = until_then;
                        html! {
                            <>
                            { timer_entry(d, "Days") }
                            { timer_entry(h, "Hours") }
                            { timer_entry(m, "Minutes") }
                            { timer_entry(s, "Seconds") }
                            </>
                        }
                    }
                    None => html! {<p>{"Now!"}</p>},
                };

                html! {
                    <>
                    <div class="centered">
                        { inner }
                    </div>
                    <div class="centered">
                        <img src="adhd.gif" class="centered"/>
                    </div>
                    </>
                }
            }
            State::Loading => {
                html! {}
            }
            State::Err => {
                html! {<p>{ "Error lmao" }</p>}
            }
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Message::UpdateTime => true,
            Message::LoadConfig(when) => {
                let interval_handle = {
                    let link = ctx.link().clone();
                    Interval::new(500, move || link.send_message(Message::UpdateTime))
                };
                self.state = State::Loaded {
                    when,
                    _interval_handle: interval_handle,
                };
                true
            }
            Message::Error => {
                self.state = State::Err;
                true
            }
        }
    }
}

async fn load_config(mut prefix: String) -> Result<DateTime<Utc>, LoadErr> {
    prefix.push_str("timerconfig.json");
    let url = prefix;
    let config: Config = Request::get(&url).send().await?.json().await?;

    let local = config
        .timezone
        .from_local_datetime(&config.datetime)
        .earliest()
        .ok_or(LoadErr::Datetime)?;

    let utc = local.with_timezone(&Utc);
    Ok(utc)
}

fn timer_entry<N>(number: N, label: &str) -> Html
where
    N: Into<u64> + Display,
{
    let number = format!("{number:02}");
    html! {
        <div>
            <p class="timer-number">{number}</p>
            <p class="timer-label">{label}</p>
        </div>
    }
}

fn secs_remaining(utc_time: DateTime<Utc>) -> Option<NonZeroU64> {
    let now = Utc::now();
    let until_then: Duration = utc_time - now;
    let tot_s = until_then.num_seconds().max(0) as u64;
    NonZeroU64::new(tot_s)
}

#[derive(Deserialize)]
struct Config {
    timezone: Tz,
    datetime: NaiveDateTime,
}

#[derive(From, Error, Display, Debug)]
enum LoadErr {
    RequestErr(gloo_net::Error),
    Datetime,
}

fn main() {
    yew::Renderer::<App>::new().render();
}
